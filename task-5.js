"use strict";

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const { name: bookName, author: bookAuthor } = bookToAdd;

const allBooks = [
    ...books,
    { bookName, bookAuthor }
]

console.log(allBooks);